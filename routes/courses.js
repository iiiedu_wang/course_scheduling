var express = require('express');
var router = express.Router();
const MongoClient = require('mongodb').MongoClient;
// Connection URL
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'scheduling';

//http://localhost:3000/api/courses
/* GET users listing. */

router.route('/')
    .post(function (req, res) {
        //console.log('post : ' + JSON.stringify(req.body));
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            const dbo = db.db(dbName);
            // const course = [{
            //     "CourseID": "DWB0012",
            //     "CourseName": "網頁設計入門",
            //     "ShortName": "HTML"
            // },{
            //     "CourseID": "DWB0012",
            //     "CourseName": "網頁設計入門",
            //     "ShortName": "HTML"
            // }];
            // const course = {
            //     "CourseID": "DWB0012",
            //     "CourseName": "網頁設計入門",
            //     "ShortName": "HTML"
            // };
            // dbo.collection("courses").insertMany(JSON.parse(req.body), function (err, res) {
            //     if (err) throw err;
            //     res.send(res)
            //     db.close();
            // });
            // console.log(req.body)
            //新增
            // dbo.collection("courses").insertOne(req.body, function (err, doc) {
            //     if (err) throw err;
            //     res.send(doc)
            //     db.close();
            // });

            //新增或修改
            dbo.collection("courses").update(
                { ClassId: "EEIT17" },
                {$set: req.body},
                { upsert: true },
                function (err, result) {
                    if (err) throw err;
                    res.send(result)
                    db.close();
                }
            );
        });

        // res.send("post course")

    })
    .get(function (req, res) {
        // res.send("get all courses")
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            const dbo = db.db(dbName);

            dbo.collection("courses").find({}).toArray(function (err, docs) {
                res.send(docs);
                db.close();
            })


        });


    });

router.route('/:classId')
    .get(function (req, res) {
        // res.send("get course：" + req.params.courseId)
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            const dbo = db.db(dbName);

            dbo.collection("courses").find({ "ClassId": req.params.classId }).toArray(function (err, docs) {
                res.send(docs);
                db.close();
            })


        });
    })
    .put(function (req, res) {
        res.send("update course：" + req.params.classId);

    })
    .delete(function (req, res) {
        res.send("delete course：" + req.params.classId);

    });

module.exports = router;