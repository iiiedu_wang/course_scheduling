//每個導師可能會進行多個班級的排課作業
function tempClass() {
    let courses = $(document.createDocumentFragment());
    for (var i = 0, max = localStorage.length; i < max; i++) {
        const btn = $('<button></button').addClass('btn btn-success mr-3').text(localStorage.key(i));
        courses.append(btn);
    }
    return courses
}

//讀取TOMS中要排課的課程資料
//從TOMS取得，可以根據排課登入者的姓名以及今天以後開課的班級進行搜尋
//SELECT [ClassSN],[ClassTypeID],[ClassID],[ClassName],[ClassIDNo],[StartDate],[EndDate],[Date],[TimesType],[ClassRoom],[ClassMentor],[Maintainer],[Length] FROM [dbo].[Class] WHERE ClassMentor = '登入者姓名' and startDate >= getDate()
//從上面的範例會取得ClassID，再根據ClassID取得這個班級的課程資料
//SELECT [Sn],[ClassID],[ClassName],[ClassTypeID],[CourseID],[CourseName],[CourseName_en],[TeacherPersonID],[TeacherName],[Length],[UserID],[LastUpdateTime] FROM [dbo].[Class_Course] WHERE ClassID = 'JJEEITT2008'
async function getTOMSCourses(theClass) {
    //theClass是Class資料表中的ClassIDNo的欄位
    //目前範例是直接讀取datas資料夾中的classcourse.json   
    let response = await fetch('datas/classcourse.json')
    let datas = await response.json();
    return datas;
}


//顯示某個班級所有的課程資料
function show(theClass) {
    const courses = JSON.parse(localStorage.getItem(theClass));
    let fragment = $(document.createDocumentFragment());
    $.each(courses, function (idx, course) {
        const cell1 = $('<td></td>').text(course.CourseID)
        const cell2 = $('<td></td>').text(course.CourseName)
        const cell3 = $('<td></td>').prop('contenteditable', 'true').text(course.ShortName)
        const cell4 = $('<td></td>').prop('contenteditable', 'true').text(course.Instructor)
        const cell5 = $('<td></td>').text(course.Length)
        const row = $('<tr></tr>').append([cell1, cell2, cell3, cell4, cell5])
        fragment.append(row)
    });
    return fragment;
}


//按下[暫存]先將資料儲存在localStorage中
function saveTempDatas(theClass) {  
    let courses = [];
    $('#courses>tbody tr').each(function () {
        let newcourse = {}
        newcourse["CourseID"] = $(this).children("td:nth-child(1)").text();
        newcourse["CourseName"] = $(this).children("td:nth-child(2)").text();
        newcourse["ShortName"] = $(this).children("td:nth-child(3)").text();
        newcourse["Instructor"] = $(this).children("td:nth-child(4)").text();
        newcourse["Length"] = $(this).children("td:nth-child(5)").text();
        courses.push(newcourse)
    })
    localStorage.setItem(theClass, JSON.stringify(courses));
}

//按下[儲存]將資料儲存到MongoDB中
function saveDatas(theClass) {  
    let courses = [];
    $('#courses>tbody tr').each(function () {
        let newcourse = {}
        newcourse["CourseID"] = $(this).children("td:nth-child(1)").text();
        newcourse["CourseName"] = $(this).children("td:nth-child(2)").text();
        newcourse["ShortName"] = $(this).children("td:nth-child(3)").text();
        newcourse["Instructor"] = $(this).children("td:nth-child(4)").text();
        newcourse["Length"] = $(this).children("td:nth-child(5)").text();
        courses.push(newcourse)
    })
    let ClassObj = {};
    ClassObj["ClassId"] = theClass
    ClassObj["Courses"] = courses;


    const url = 'http://localhost:3000/api/courses/';
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(ClassObj),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(res => res.json())
        .catch(error => Swal.fire(error))
        .then(datas => {
            localStorage.removeItem(theClass);
            Swal.fire('新增成功')
        });
}

//排課時使用
async function showCourses(theClass) {
  let datas = await getServerCourses(theClass)
  let fragment = $(document.createDocumentFragment());
    $.each(datas[0].Courses, function (idx, course) {
        const rdo = $('<input>').attr({ 'type': 'radio', 'name': 'course', 'data-length': course.Length }).val(course.ShortName).addClass('form-check-input');
        const lbl = $('<label></label>').addClass('form-check-label').html("<span>" + course.ShortName + "(" + course.Length + "hrs)</span>").prepend(rdo)
        const item = $('<li></li>').addClass('list-group-item').html(lbl)
        fragment.append(item)
    })
    return fragment;
}

async function getServerCourses(theClass) {
    const url = `http://localhost:3000/api/courses/${theClass}`;
    let response = await fetch(url);
    let datas = await response.json();
    return datas;
}

//先從MongoDB中看看有無班級的課程資料
//如果沒有再從TOMS中取回班級的課程資料
async function getCourses(theClass) {
    let datas = await getServerCourses(theClass);
    if (Array.isArray(datas) && datas.length !== 0) {
        localStorage.setItem(theClass, JSON.stringify(datas[0].Courses));
    } else {
        let datas = await getTOMSCourses(theClass);
        if (Array.isArray(datas) && datas.length !== 0) {
            localStorage.setItem(theClass, JSON.stringify(datas));
        } else {
            Swal.fire("無此班級的課程資料")
        }
    }

}
